# Notes rapides

Pouvoir faire une note vocale rapidement depuis le téléphone. 
Et les organiser pour
- organiser des connaissances (base de connaissances)
- publier du contenu (blog, vidéo, podcast, indieweb)
- faire des événements (ical, mobilizon)

## Use case

**Créer** (empiler) une note vocale

1. Trigger (par voix à la OK google, ou par appui sur nouveau)
2. mode écoute (écoute la voix jusqu'à la fin de voix ou STOP NOTE ou appui STOP)
3. Sauvegarde métadata + son dans un fichier


**Dépiler**

1. Action dépiler (vocal ou bouton)
2. lecture de la note
3. call to action
  - supprimer
  - archiver (pour journalisation)
    - juste métadonnées
    - transcription
  - organiser
    - Article
    - Evénement

**Triage**

Prendre les différentes notes et les dispatcher au bon endroit

## Requis

- Apprendre tuto de base Android
- Ecouter la voix
- Sauvegarder des données
  - [Data persistence](https://developer.android.com/courses/android-basics-kotlin/unit-5) 
- Barre de notification avec bouton (pour lancer un enregistrement même verouillé)
  - Notification.MediaStyle


